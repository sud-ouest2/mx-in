#!/bin/bash
# Script Licence: GNU/GPL v3
#   2017 - Eric Seigne <eric.seigne@abuledu.org>
# automatic renew let's encrypt certificates

# http-01 is used -> nginx help
service nginx start

dehydrated -c

service postfix reload
service nginx stop
