# Configuration et scripts pour le/les serveurs MX entrant

L'infrastructure de sud-ouest2.org est prévue pour avoir plusieurs serveurx MX entrant, vous trouverez donc dans ce dépot les fichiers de configuration et / ou les scripts utilisés pour ça

Le principe est le suivant:
* La gestion administrative est gérée par Dolibarr
* Le système central est géré par Modoboa
* Un ou plusieurs serveurs MX frontaux

Sur le serveur MX frontal
* On a une liste de domaines dont on est destinataire -> voir dolibarr
* On a ensuite une liste de comptes dont on accepte les mails -> voir modoboa
* Modoboa propose en standard plein de choses via des requêtes SQL -> on réutilise

## Installation OS

* OS de base : Debian GNU/Linux
* Paquets logiciels installés : voir le contenu du fichier dpkg.get_selections

## Configuration postfix
* Voir les fichiers présents dans le dépot

## Scripts personnalisés

Tous les scripts personnalisés sont dans /usr/local/bin :

* soo2-update_certs.sh et son cron associé mettent à jour les certificats SSL via let's encrypt
* soo2-update_relays.sh et son cron associé mettent à jour la liste des règles de relais pour les listes de diffusions qui ne sont pas gérées par modoboa
* soo2-update_transport_listes.sh et son cron s'occupent de faire les règles de routage pour les listes de diffusions

## Certificats Let's encrypt pour postfix

Pour que postfix soit accessible en ssl/tls (trafic serveur-serveur) nous avons mis en place un certificat Let's Encrypt.

* Voir /usr/local/bin/soo2-update_certs.sh et son cron associé
* Voir la configuration main.cf de postfix smtp_tls et smtpd_tls
