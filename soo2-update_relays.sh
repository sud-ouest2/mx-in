#!/bin/bash
# Script Licence: GNU/GPL v3
#   2017 - Eric Seigne <eric.seigne@abuledu.org>
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#comme on est relais il faut qu'on sache quels sont les domaines virtuels qu'on heberge ...
#requete sur dolibarr pour avoir la liste ... https://sud-ouest2.org/dolibarr/public/soo/get_liste_virtualhosts.php
FIC=`tempfile`

LADATE=`date "+%Y%m%d %H:%M"`
echo "------------- ${LADATE}"

wget -q https://sud-ouest2.org/dolibarr/public/soo/get_liste_virtualhosts.php -O ${FIC}
if [ $? != 0 ]; then
    #erreur de transfert ou autre
    echo " erreur wget ... -> exit"
    exit
fi

#pour ne pas tout refaire si pas de nouveautes
diff ${FIC} /etc/postfix/relaydomains.reference
if [ $? == 0 ]; then
    #aucune modif -> exit
    echo " aucune modification depuis la derniere fois -> exit"
    rm ${FIC}
    exit
fi

echo "il y a des modification depuis la derniere fois ..."
rm /etc/postfix/relaydomains.reference
rm /etc/postfix/relaydomains
rm /etc/postfix/relaydomains.db
for domaine in `cat ${FIC} /etc/postfix/relaydomains.orig | sort -u`
do
    echo "  $domaine [ajout]"
    echo "$domaine	OK" >> /etc/postfix/relaydomains
done

cp ${FIC} /etc/postfix/relaydomains.reference

echo "construction de la base de type hash pour /etc/postfix/relaydomains"
/usr/sbin/postmap hash:/etc/postfix/relaydomains
chmod 644 /etc/postfix/relaydomains*

rm ${FIC}

echo "restart de postfix ..."
service postfix restart
echo "the end"
